﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using DataLayer.Data;
using DataLayer.Models;
using DataLayer.Interfaces;

namespace DataLayer.Repository
{
    public class RepositoryPerson : IRepository<Person>
    {
        AppDbContext _dbcontext;

        public RepositoryPerson(AppDbContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        public async Task<Person> add(Person person)
        {
            var obj = await _dbcontext.Person.AddAsync(person);
            _dbcontext.SaveChanges();
            return obj.Entity;
        }

        public IEnumerable<Person> getAll()
        {
            return _dbcontext.Person
                .Where(x => x.isdeleted == false)
                .ToList();
        }

        public List<Person> getByName(string username)
        {
            return _dbcontext.Person
                .Where(x => x.isdeleted == false && x.username == username)
                .ToList();
        }

        public void update(Person person)
        {
            _dbcontext.Update(person);
            _dbcontext.SaveChanges();
        }

        public void delete(Person person)
        {
            _dbcontext.Remove(person);
            _dbcontext.SaveChanges();
        }

        public PagedList<Person> getAllPaged(PersonParameter personParameter)
        {
            return PagedList<Person>.ToPagedList(_dbcontext.Person
                .Where(x => x.isdeleted == false),
                personParameter.PageNumber,
                personParameter.PageSize);
            /*
            //Ganti PagedList jadi IEnumerable
            return _dbcontext.Person
                .Where(x => x.isdeleted == false)
                .Skip((personParameter.PageNumber - 1) * personParameter.PageSize)
                .Take(personParameter.PageSize)
                .ToList();
            */
        }
    }
}
