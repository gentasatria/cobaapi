﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Models
{
    public class Person
    {
        public int id { get; set; }

        public string username { get; set; }

        public string userpassword { get; set; }

        public string useremail { get; set; }

        public DateTime createdon { get; set; } = DateTime.Now;

        public bool isdeleted { get; set; } = false;
    }
}
