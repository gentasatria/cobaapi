﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using DataLayer.Models;
using DataLayer.Data;

namespace DataLayer.Interfaces
{
    public interface IRepository<T>
    {
        public Task<T> add(T obj);

        public IEnumerable<T> getAll();

        public List<T> getByName(string username);

        public void update(T obj);

        public void delete(T obj);
        public PagedList<Person> getAllPaged(PersonParameter personParameter);
    }
}
