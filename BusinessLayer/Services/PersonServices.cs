﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

using DataLayer.Models;
using DataLayer.Interfaces;
using DataLayer.Data;

namespace BusinessLayer.Services
{
    public class PersonServices
    {
        private readonly IRepository<Person> _person;

        public PersonServices(IRepository<Person> person)
        {
            _person = person;
        }

        //Add person
        public async Task<Person> addPerson(Person obj)
        {
            return await _person.add(obj);
        }

        //Get all person
        public IEnumerable<Person> getAllPerson()
        {
            try
            {
                return _person.getAll().ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Get Person By Username
        public List<Person> getPersonByName(string username)
        {
            return _person.getByName(username);
        }

        //Update Person
        public bool updatePerson(Person obj)
        {
            try
            {
                var personList = _person.getByName(obj.username);
                foreach (var person in personList)
                {
                    _person.update(person);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Delete Person
        public bool deletePerson(Person obj)
        {
            try
            {
                var personList = _person.getByName(obj.username);
                foreach (var person in personList)
                {
                    _person.delete(person);
                }
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        //Get All Person but paged
        public PagedList<Person> getAllPaged(PersonParameter parameter)
        {
            try
            {
                return _person.getAllPaged(parameter);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
