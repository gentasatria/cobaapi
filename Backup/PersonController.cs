﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using DataLayer.Data;
using DataLayer.Models;

using BusinessLayer.Services;

namespace CobaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly AppDbContext _dbcontext;

        private List<string> _names;
        private int _lenNames;
        private Random _rand;
        //public PersonController() { }

        public PersonController(AppDbContext dbcontext)
        {
            _dbcontext = dbcontext;
            _rand = new Random();
            _names = new List<string>()
            {
                "aku", "adalah", "anak", "gembala",
                "akuadalah", "anakgembala", "akuadalahanak", "akuadalahanakgembala"
            };
            _lenNames = _names.Count;
        }

        private Person PersonRandomizer()
        {
            int _randname = _rand.Next(0,_lenNames);
            int _randpass = _rand.Next(0,_lenNames);
            Person _person = new Person()
            {
                username = _names[_randname],
                useremail = _names[_randname] + "@gmail.com",
                userpassword = _names[_randpass],
                isdeleted = false,
                createdon = DateTime.Now
            };
            return _person;
        }

        [HttpGet("HelloWorld")]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [HttpGet("AddPerson")]
        public async Task<Person> addPerson()
        {
            var obj = await _dbcontext.Person.AddAsync(PersonRandomizer());
            _dbcontext.SaveChanges();
            return obj.Entity;
        }

        [HttpGet("GetPerson")]
        public List<Person> getAll()
        {
            return _dbcontext.Person.Where(x => x.isdeleted == false).ToList();
        }

        [HttpGet("GetPerson/{username}")]
        public List<Person> getByName(string username)
        {
            return _dbcontext.Person.Where(x => x.isdeleted == false && x.username == username).ToList();
        }

        [HttpGet("UpdatePerson/{username}")]
        public string UpdatePerson(string username)
        {
            try
            {
                var personList = getByName(username);
                foreach (var person in personList)
                {
                    person.useremail = _names[_rand.Next(0, _lenNames)] +"@gmail.com";
                    person.userpassword = _names[_rand.Next(0, _lenNames)];
                    _dbcontext.Update(person);
                    _dbcontext.SaveChanges();
                }
                return username + " updated successfully"; ;
            }
            catch (Exception)
            {
                return "Failed to update " + username;
            }
        }

        [HttpGet("DeletePerson/{username}")]
        public string DeletePerson(string username)
        {
            try
            {
                var personList = getByName(username);
                foreach (var person in personList)
                {
                    _dbcontext.Remove(person);
                    _dbcontext.SaveChanges();
                }
                return username + " deleted successfully";
            }
            catch (Exception)
            {
                return "Failed to delete " + username;
            }
        }
    }
}
