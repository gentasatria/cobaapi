﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using DataLayer.Data;
using DataLayer.Models;
using DataLayer.Interfaces;

using BusinessLayer.Services;

namespace CobaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : Controller
    {
        private readonly PersonServices _personServices;
        private readonly IRepository<Person> _personRepository;

        private Random _rand;
        private List<string> _names;
        private int _lenNames;
        //public PersonController() { }

        public PersonController(PersonServices personServices, IRepository<Person> personRepository)
        {
            _personServices = personServices;
            _personRepository = personRepository;

            _rand = new Random();
            _names = new List<string>()
            {
                "aku", "adalah", "anak", "gembala",
                "akuadalah", "anakgembala", "akuadalahanak", "akuadalahanakgembala"
            };
            _lenNames = _names.Count;
        }

        private Person PersonRandomizer()
        {
            int _randname = _rand.Next(0,_lenNames);
            int _randpass = _rand.Next(0,_lenNames);
            Person _person = new Person()
            {
                username = _names[_randname],
                useremail = _names[_randname] + "@gmail.com",
                userpassword = _names[_randpass],
                isdeleted = false,
                createdon = DateTime.Now
            };
            return _person;
        }

        [HttpGet("HelloWorld")]
        public string HelloWorld()
        {
            return "Hello World";
        }
        
        [HttpGet("Welcome")]
        public IActionResult Welcome()
        {
            Person person = PersonRandomizer();
            return View("Views/Welcome.cshtml",person);
        }

        [HttpGet("")]
        public IActionResult Index(int PageNumber = 1, int PageSize = 5)
        {
            PersonParameter parameter = new PersonParameter() { PageNumber = PageNumber, PageSize = PageSize};
            PagedList<Person> pagedList = _personServices.getAllPaged(parameter);
            Console.WriteLine(pagedList.CurrentPage);
            return View("Views/Index.cshtml", pagedList);
        }

        [HttpGet("Add")]
        public IActionResult add()
        {
            return View("Views/Add.cshtml");
        }
        
        [HttpGet("AddPerson")]
        public async Task<Object> addPerson(string uname, string email, string password)
        {
            if(uname == null || email == null || password == null)
            {
                return false;
            }
            try
            {
                //await _personServices.addPerson(PersonRandomizer());
                Person person = new Person()
                {
                    username = uname,
                    userpassword = password,
                    useremail = email,
                    createdon = DateTime.Now,
                    isdeleted = false
                };
                await _personServices.addPerson(person);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpGet("GetPerson")]
        public PagedList<Person> getAll(int PageNumber = 1, int PageSize = 5)
        {
            PersonParameter parameter = new PersonParameter() { PageNumber = PageNumber, PageSize = PageSize};
            PagedList<Person> pagedList = _personServices.getAllPaged(parameter);
            Console.WriteLine(pagedList.CurrentPage);
            return _personServices.getAllPaged(parameter);
        }

        [HttpGet("Detail")]
        public IActionResult Detail(string username)
        {
            var person = _personServices.getPersonByName(username);
            return View("Views/Detail.cshtml", person);
        }

        [HttpGet("GetPerson/{username}")]
        public List<Person> getByName(string username)
        {
            return _personServices.getPersonByName(username).ToList();
        }

        [HttpGet("Update")]
        public IActionResult update(string username)
        {
            var person = _personServices.getPersonByName(username);
            return View("Views/Update.cshtml", person[0]);
        }

        [HttpGet("UpdatePerson")]
        public string UpdatePerson(string uname, string email, string password)
        {
            try
            {
                List<Person> personList = _personServices.getPersonByName(uname);
                foreach (var person in personList)
                {
                    person.useremail = email;
                    person.userpassword = password;
                    _personServices.updatePerson(person);
                }
                return uname + " updated successfully"; ;
            }
            catch (Exception)
            {
                return "Failed to update " + uname;
            }
        }

        [HttpGet("Delete")]
        public string delete(string username)
        {
            try
            {
                List<Person> personList = _personServices.getPersonByName(username);
                if(personList.Count == 0)
                {
                    return username + "not found!";
                }

                foreach (var person in personList)
                {
                    person.useremail = _names[_rand.Next(0, _lenNames)] + "@gmail.com";
                    person.userpassword = _names[_rand.Next(0, _lenNames)];
                    _personServices.deletePerson(person);
                }
                return username + " deleted successfully";
            }
            catch (Exception)
            {
                return "Failed to delete " + username;
            }
        }

        [HttpGet("DeletePerson/{username}")]
        public string DeletePerson(string username)
        {
            try
            {
                List<Person> personList = _personServices.getPersonByName(username);
                foreach (var person in personList)
                {
                    person.useremail = _names[_rand.Next(0, _lenNames)] + "@gmail.com";
                    person.userpassword = _names[_rand.Next(0, _lenNames)];
                    _personServices.deletePerson(person);
                }
                return username + " deleted successfully";
            }
            catch (Exception)
            {
                return "Failed to delete " + username;
            }
        }
    }
}
